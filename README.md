# 1. Simple full-mesh topology

# 1.1 Network virtualisation: Mininet

* Open the definition of the Mininet network under /opt/pyladies/net/fullmesh.py and examine the "HostToSwitchAndFullMesh" class and the main method
* Run full-mesh topo:

  > ./1a_net.sh

* Observe the output. Type in commands "dump", "net", "links"... (hit tab for more commands).
  More details: https://github.com/mininet/mininet/wiki/Introduction-to-Mininet#working-with-mininet
* Type "xterm h1 h2" and inside each terminal ping each other (or, alternatively, type "h1 ping h2"):

  h1> ping 10.0.0.2
  h2> ping 10.0.0.1

  Notice how there is no response at the moment. These hosts cannot connect

# 1.2 Network controller: Ryu

* Open the Ryu controller logic under /opt/pyladies/ctrl/ryu_manual.py and examine the "_packet_in_handler" method
* Run Ryu controller:

  > ./1b_ctrl.sh

# 1.3 Network virtualisation: Mininet

* Get back again to the xterm terminals in Mininet. This pair of hosts should have become communicated at this point
* Bonus: consider getting back to the Ryu controller app and set "TIMEOUT_ENABLED" to True, then restart. After ~10 seconds you should notice an interruption in the connectivity. Important: kill properly (-9!) the Ryu process and restart the Mininet process to make sure there is no trace of old logic before

# 1.4 Clean-up

* After you are finished testing, do terminate both running processes ("exit" in Mininet or Ctrl+Z on both Mininet and Ryu controller)


# 2. Tagged topology

# 2.1 Network virtualisation: Mininet

* Open the definition of the Mininet network under /opt/pyladies/net/vpls.py and examine the whole script
* Run the tagged topo:

  > ./2a_net.sh

* Observe the output
* Type "xterm h1 h2 h3 h5 h6" and inside each terminal ping as defined below (or, alternatively, type e.g., "h1 ping h2"):

  h1> ping 10.0.0.2
  h2> ping 10.0.0.1
  h3> ping 10.0.0.5
  h5> ping 10.0.0.6
  h6> ping 10.0.0.5

  Notice how there is no response at the moment. No pair of hosts can connect

# 2.2 Network controller: ONOS

* Run the ONOS controller:

  > ./2b_ctrl.sh

* Wait for around ~1 minute. Then, open another terminal and configure it:

  > ssh-keygen -f "/home/pyladies/.ssh/known_hosts" -R [localhost]:8101
  > ssh karaf@localhost -p8101

  Access with password "karaf".
  Once in the console, disable the "fwd" app (which can find a way to automatically connect the hosts). This will be done manually instead:

  > app deactivate org.onosproject.fwd
  > app activate org.onosproject.vpls

  Now, this enables the "vpls" app (Virtual Private Layer Service). This allows to create layers on top of the topology and to define subsets of hosts that can be connected

  Define the interfaces in use (also with the tag/VLAN in use):

  > interface-add -v 100 of:0000000000000001/1 h1
  > interface-add -v 200 of:0000000000000002/1 h2
  > interface-add -v 300 of:0000000000000004/1 h3
  > interface-add -v 400 of:0000000000000002/1 h4
  > interface-add of:0000000000000005/1 h5
  > interface-add of:0000000000000006/1 h6

  Configure the layers by defining its encapsulation type and the containing interfaces. Important: copy and paste (or type and use tabs for) each line separately:

  > vpls create layer1
  > vpls add-if layer1 h1
  > vpls add-if layer1 h2
  > vpls set-encap layer1 VLAN

  > vpls create layer2
  > vpls add-if layer2 h3
  > vpls add-if layer2 h5
  > vpls add-if layer2 h6

  > vpls show layer2
  > vpls show layer1

  There are two different layers: layer1=[h1,h2] and layer2=[h5,h6,h3].
  The first layer connects a pair of hosts that tag their packets using VLANs. This layer does use VLAN encapsulation.
  The second later connects three, where two are untagged and the third one is. This layer does not use VLAN encapsulation.
  Note: the latter can be a problem: h3 sends packets in a different way

# 2.3 Network virtualisation: Mininet

* Get back again to the xterm terminals in Mininet. The pairs of hosts (h1,h2) and (h5,h6) should be now connected. The former should send VLAN-tagged TCP packets, whilst the latter should send plain TCP packets. Feel free to identify and inspect the interfaces through which the ICMP traffic is sent (e.g., with "tcpdump -i ${interface}), or to generate TCP traffic (iperf -s, iperf -c ${server_ip})

# 2.4 Topology check

* Access the GUI in http://${YOUR_IP}:8181/onos/ui with user "onos" and password "rocks"
* Check the graph, click on the items, investigate information on the devices, their flows, etc

# 2.5 Clean-up

* After you are finished testing, do terminate both running processes ("exit" in Mininet, "logout" in Karaf, /opt/pyladies/ctrl/onos_stop.sh for the ONOS controller)
