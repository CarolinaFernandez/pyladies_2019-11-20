#!/bin/bash

mode=${1:-"fg"}

sudo mn -c

if [[ $mode == "bg" ]]; then
  sudo python /opt/pyladies/net/vpls.py
elif [[ $mode == "fg" ]]; then
  sudo mn --custom /opt/pyladies/net/vpls.py --topo vpls --switch ovs,protocols=OpenFlow13,stp=1 --controller=remote,ip=127.0.0.1,port=6633 --mac
fi
