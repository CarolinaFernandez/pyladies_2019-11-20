#!/usr/bin/python

import sys
sys.path.append("/opt/mininet")

from mininet.link import TCLink
from mininet.log import setLogLevel
from mininet.net import Mininet
from mininet.node import CPULimitedHost
from mininet.node import RemoteController
from mininet.topo import Topo
from mininet.util import dumpNodeConnections

class HostToSwitchAndFullMesh(Topo):
    "N switches, each connected to a host and to all other switches."
    def build(self, n=4):
        switches = []
        hosts = []
        for h in range(1, n+1):
            switch = self.addSwitch("s%s" % h, ip="10.0.1.%s" % h, mac="00:00:00:00:01:0%s" % h)
            switches.append(switch)
            # Each host gets 50%/n of system CPU
            host = self.addHost("h%s" % h, ip="10.0.0.%s" % h, mac="00:00:00:00:00:0%s" % h, cpu=.5/n)
            hosts.append(host)
            # 10 Mbps, 5ms delay, 2% loss, 1000 packet queue
            self.addLink(host, switch, bw=10, delay="5ms", loss=2, max_queue_size=1000, use_htb=True)
        links_added = []
        for i, switch_i in enumerate(switches):
            for j, switch_j in enumerate(switches):
                if i != j and (i, j) not in links_added and (j, i) not in links_added:
                    links_added.append((i, j))
                    self.addLink(switch_i, switch_j, bw=10, delay="10ms", loss=2, max_queue_size=1000, use_htb=True)

def get_host_ifconfig(topo, net):
    hosts = filter(lambda x: x.startswith("h"), topo.nodes())
    for host_name in hosts:
        host = net.get(host_name)
        result = host.cmd("ifconfig")
        print(result)

def performance_test(topo, net):
    "Create network and run simple performance test"
    net.start()
    print("Dumping host connections")
    dumpNodeConnections(net.hosts)
    print("Testing network connectivity")
    net.pingAll()

topos = { "fullmesh" : HostToSwitchAndFullMesh }

if __name__ == "__main__":
    setLogLevel("info")
    topo = HostToSwitchAndFullMesh(4)
    net = Mininet(topo)
    # Connect switches to controller
    local_controller = net.addController("controller0", controller=RemoteController, ip="127.0.0.1", port=6633)
    get_host_ifconfig(topo, net)
    performance_test(topo, net)
