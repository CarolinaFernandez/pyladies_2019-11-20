#!/bin/bash

mode=${1:-"fg"}
num_switches=${2:-4}

sudo mn -c

if [[ $mode == "bg" ]]; then
  sudo python fullmesh.py ${num_switches}
elif [[ $mode == "fg" ]]; then
  sudo mn --custom fullmesh.py --topo fullmesh,${num_switches} --switch ovs,protocols=OpenFlow13,stp=1 --controller=remote,ip=127.0.0.1,port=6633 --mac
fi
