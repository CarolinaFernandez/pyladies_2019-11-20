#!/bin/bash

docker run -it --rm \
  -e ONOS_APPS=gui2,openflow,lldpprovider,hostprovider,vpls \
  -p 8101:8101 -p 8181:8181 -p 6633:6633 \
  --name "onos" \
  onosproject/onos:2.2.x
