#!/bin/bash

for pid in $(pgrep ryu-manager) ; do echo "killing pid=$pid"; kill -9 $pid ; done
